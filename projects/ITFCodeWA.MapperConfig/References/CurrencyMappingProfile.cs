﻿using ITFCodeWA.Data.References;
using ITFCodeWA.MapperConfig.Base.MappingProfiles;
using ITFCodeWA.Models.References;

namespace ITFCodeWA.MapperConfig.References
{
    public class CurrencyMappingProfile : ReferenceMappingProfile<Currency, CurrencyModel>
    {
    }
}
