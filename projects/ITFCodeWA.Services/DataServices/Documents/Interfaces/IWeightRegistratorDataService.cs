﻿using ITFCodeWA.Core.Services.DataServices.Base.Interfaces;
using ITFCodeWA.Models.Documents;

namespace ITFCodeWA.Services.DataServices.Documents.Interfaces
{
    public interface IWeightRegistratorDataService : IDocumentDataService<WeightRegistratorModel>
    {
    }
}