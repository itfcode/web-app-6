﻿using ITFCodeWA.Core.Data.References;

namespace ITFCodeWA.Data.References
{
    public class PreciousMetal : ReferenceBase
    {
        public string? LatinName { get; set; }
    }
}