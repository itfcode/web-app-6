﻿using System;
using System.Linq;

namespace ITFCodeWA.API.Constants
{
	public static class SwaggerConfiguration1
	{
		public static class Person 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class Contract 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class Contractor 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class Currency 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class MoneyAccount 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class ExpenseItem 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class ExpenseGroup 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class RevenueItem 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class RevenueGroup 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class Good 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class GoodGroup 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class Food 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
		public static class FoodGroup 
		{
			public const string TAG_DESCRIPTION = "";

			public const string GET_RESPONSE_DESCRIPTION = "Получение информация о R";
            public const string GET_OPERATION_SUMMARY = "GET-OPERATION SUMMARY";
            public const string GET_OPERATION_DESCRIPTION = "GET-OPERATION DESCRITION";
            public const string GET_ID_PARAMTER_DESCRIPTION = "GET-ID-PARAMTER-DESCRIPTION";

            public const string POST_RESPONSE_DESCRIPTION = "Внесение информации о R";
            public const string POST_OPERATION_SUMMARY = "POST-OPERATION SUMMARY";
            public const string POST_OPERATION_DESCRIPTION = "POST-OPERATION DESCRI TION";

            public const string PUT_RESPONSE_DESCRIPTION = "Обноление информация о R";
            public const string PUT_OPERATION_SUMMARY = "PUT-OPERATION SUMMARY";
            public const string PUT_OPERATION_DESCRIPTION = "PUT-OPERATION DESCRITION";
            public const string PUT_ID_PARAMTER_DESCRIPTION = "PUT-PARAMTER-ID";

            public const string DELETE_RESPONSE_DESCRIPTION = "Уделение информация о R";
            public const string DELETE_OPERATION_SUMMARY = "DELETE-OPERATION SUMMARY";
            public const string DELETE_OPERATION_DESCRIPTION = "DELETE-OPERATION DESCRITION";
            public const string DELETE_ID_PARAMTER_DESCRIPTION = "DELETE-ID-PARAMTER-DESCRIPTION";

            public const string GET_PAGE_RESPONSE_DESCRIPTION = "Информация о R";
            public const string GET_PAGE_OPERATION_SUMMARY = "GET-PAGE-OPERATION SUMMARY";
            public const string GET_PAGE_OPERATION_DESCRIPTION = "GET-PAGE-OPERATION DESCRITION";
		}
	}
}