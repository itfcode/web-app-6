import { IEntitySyncModel } from "./imodel";

export interface IDocumentRowModel extends IEntitySyncModel {
  documentId: string;
}
