﻿using ITFCodeWA.Core.Domain.EntityConfigurations;
using ITFCodeWA.Data.References;

namespace ITFCodeWA.Domain.EntityConfigurations.References
{
    public class GoodGroupConfiguration : ReferenceConfiguration<GoodGroup>
    {
        public override void Configure()
        {
            base.Configure();
        }
    }
}
