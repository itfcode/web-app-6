﻿using ITFCodeWA.Core.Domain.Repositories.References.Interfaces;
using ITFCodeWA.Data.References;

namespace ITFCodeWA.Domain.Repositories.References.Interfaces
{
    public interface IGoodGroupRepository : IReferenceRepository<GoodGroup>
    {
    }
}