﻿using ITFCodeWA.Core.Models.Common.Documents;
using ITFCodeWA.Core.Models.Common.Documents.Interfaces;

namespace ITFCodeWA.Models.Health.Documents.Base
{
    public class RegistratorRowBaseModel : DocumentRowBaseModel, IDocumentRowModel
    {
    }
}