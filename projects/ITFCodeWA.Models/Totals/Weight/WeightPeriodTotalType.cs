﻿namespace ITFCodeWA.Models.Totals.Weight
{
    public enum PeriodWeightTotalType
    {
        Year = 10,
        Month = 20,
        Week = 30
    }
}