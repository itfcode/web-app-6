﻿using ITFCodeWA.Core.Models.QueryFilters.Single.Base;

namespace ITFCodeWA.Core.Models.QueryFilters.Single
{
    public class BoolSingleFilter : QuerySingleFilter<bool>
    {
    }
}