﻿namespace ITFCodeWA.Core.Models.QueryFilters.Single
{
    public enum NumericFilterMatchMode
    {
        Equals,
        LessThan,
        GreaterThan,
        LessThanOrEquals,
        GreaterThanOrEquals,
    }
}
