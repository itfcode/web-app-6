﻿namespace ITFCodeWA.Core.Models.QueryFilters.Single
{
    public enum DateFilterMatchMode
    {
        Equals,
        LessThan,
        GreaterThan,
        LessThanOrEquals,
        GreaterThanOrEquals,
    }
}